Instance: myFancyPants 
InstanceOf: CodeSystem
Usage: #definition 
* url = "http://myFancy.url" 
* version = "1.0.0" 
* name = "myFancyPants" 
* title = "myTitleOfFancePants" 
* status = #draft 
* description = "this is my facy pants valueset" 
* content = #complete
* implicitRules = "http://asdsad"
* language = #en
* text = "fancy pants"
* contained = "http://contained something" //ToDo eigentlich ganze Ressource abbilden?!
* extension = "http://some extension"
* modifierExtension = "http://some modifierExtension"
* identifier[0].use = official
* identifier[0].system = "http://oid"
* identifier[0].value = 1.2.40.0.10.6.34.1337
* experimental = false
* date = "2019-04-02"
* publisher = "the gang"
//no contact
* useContext[0].code.code = #234
* useContext[0].code.system = "http://sfadfs"
* useContext[0].code.version = "2.0"
* useContext[0].code.display = "doggo"
* useContext[0].code.userSelected = false
* useContext[0].valueQuantity.unit = "blabla"
* jurisdiction[0].coding.code = #222
* jurisdiction[0].coding.system = "http://dfhjsdfh"
* jurisdiction[0].coding.version = "3.0.1"
* jurisdiction[0].coding.display = "hfdrtt"
* jurisdiction[0].coding.userSelected = false
* jurisdiction[0].text = "huhu"
* purpose = "for everything"
* copyright = "R"
* property[0].code = #qwer 
* property[0].uri = "http://qwer" 
* property[0].description = "this is qwer" 
* property[0].type = #code 
* property[1].code = #asdf 
* property[1].uri = "http://asdf" 
* property[1].description = "this is asdf" 
* property[1].type = #code 
