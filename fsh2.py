from fsh1 import Fsh1
import sys
import prop_csv_and_master as pcam

class Fsh2(Fsh1):

    def parse(inFilename):
        return Fsh2(inFilename)

    def parseConcepts(self, dictConcept, line, splittedLine, value):
        if '* #' in splittedLine[0]:
            theConcept = None
                        #split here for CS and VS
            if self.resource == self.resource.CodeSystem:
                theConcept = pcam.CSConcept() #alternative construktor to create an new object without calling __init__, for minimal memory allocation
                if '^' in line:
                    if oneConcept := dictConcept[line.split(' ')[1].strip('#')]:
                        if 'definition' in line:
                            oneConcept.definition = value #line.split('"')[1]
                        elif 'designation' in line:
                            theDesignation = None
                                        # look with idInFsh for designation in the designation list
                            for oneDesignation in oneConcept.designation:
                                if oneDesignation.idInFsh == line.split(']')[0]:
                                    theDesignation = oneDesignation
                                        # if none is found in the list, make one
                            if theDesignation is None:
                                theDesignation.idInFsh = line.split(']')[0]
                                oneConcept.designation.append(theDesignation)
                                        # split the line properly
                            if line.split('#') > 2:
                                value = line.strip().split[' '][-1:][0].strip('#')
                            else:
                                value = line.strip().split['"'][1]
                                        # add the data to the designation
                            if 'language' in splittedLine[0].split('.')[1].strip():
                                theDesignation.language = value
                            elif 'use' in splittedLine[0].split('.')[1].strip():
                                if 'userSelected' in splittedLine[0].split('.')[1].strip():
                                    theDesignation.use.userSelected = value
                                else:
                                    str4Coding = value
                                    if "|" in value:
                                        str4Coding = str4Coding.replace("|","^",1)
                                        str4Coding = str4Coding.replace("#","^",1)
                                    else:
                                        str4Coding = str4Coding.replace("#","^^",1)
                                    if " \"" in value:
                                        str4Coding = str4Coding.replace(" \"","^",1).replace("\"","")
                                    else:
                                        str4Coding += "^"
                                    str4Coding += "^"
                                    theDesignation.use = pcam.Coding(str4Coding)
                            elif 'value' in splittedLine[0].split('.')[1].strip():
                                theDesignation.value = value
                        elif 'property' in line:
                                        #line.split('^')[1]
                            theProperty = None
                                        # look with idInFsh for Property in the Property list
                            for oneProperty in oneConcept.property:
                                if oneProperty.idInFsh == line.split(']')[0]:
                                    theProperty = oneProperty
                                        # if none is found in the list, make one
                            if theProperty is None:
                                theProperty = pcam.CSConcept.ConceptProperty.__new__(pcam.CSConcept.ConceptProperty)
                                theProperty.idInFsh = line.split(']')[0]
                                oneConcept.property.append(theProperty)
                                        # split the line properly
                            #if len(line.split('#')) > 2:
                            #    value = line.strip().split(' ')[-1:][0].strip('#')
                            #else:
                            #    value = line.strip().split('"')[1]
                                        # add the data to the Property
                            if 'code' in splittedLine[0].strip().split('.')[-1].strip():
                                theProperty.code = value
                            elif 'value' in splittedLine[0].split('.')[-1].strip():
                                theProperty.value = value
                                theProperty.type = pcam.PropertyCodeType(splittedLine[0].split('.')[-1].strip()[5:].lower())
                else:
                                # a detection of two # (without a ^) in a line to give a syserr that nested hierarchy is not supported
                    if len(line.split('#')) > 2:
                        sys.exit('nested hierarchy per "* #this #that" is not supported, only the use of a multiparent hierarchy per propertys is supported.')
                                # go on with parsing main data of the concept
                    theConcept.code = line.split(' ')[1].strip('#')
                    if '"' in line:
                        theConcept.display = line.split('"')[1]
                    theConcept.designation = []
                    theConcept.property = []
                    dictConcept[theConcept.code] = theConcept

                        # todo get to version 2
            elif self.resource == self.resource.ValueSet:
                theConcept = pcam.VSConcept()#.__new__(pcam.VSConcept) #alternative construktor to create an new object without calling __init__, for minimal memory allocation
                if 'compose.include.system' in line:
                    finalVSSystem = line.split('"')[1].strip()
                if 'compose.include.version' in line:
                    finalVSVersion = line.split('"')[1].strip()
                if '.code' in line:
                    theConcept.idInFsh = line.split('.code')[0]
                    theConcept.code = line.split('#')[1].strip()
                    theConcept.system = finalVSSystem
                    theConcept.version = finalVSVersion
                    theConcept.designation = []
                    theConcept.filter = []
                    dictConcept[theConcept.code] = theConcept
                if '.display' in line:
                    for oneConcept in dictConcept:
                        if oneConcept.idInFsh == line.split('.display')[0]:
                            theConcept = oneConcept
                            theConcept.display = line.split('"')[1].strip()
                if '.designation' in line:
                    theDesignation = None
                    for oneConcept in dictConcept:
                        if oneConcept.idInFsh == line.split('.designation')[0]:
                                        # look with idInFsh for designation in the designation list of the concept
                            for oneDesignation in oneConcept.designation: # e.g. ->* compose.include.concept[5].designation[4].language
                                if oneDesignation.idInFsh == line.split(']')[0]+line.split(']')[1]:
                                    theDesignation = oneDesignation
                                        # if none is found in the list, make one
                            if theDesignation is None:
                                theDesignation.idInFsh = line.split(']')[0]+line.split(']')[1]
                                oneConcept.designation.append(theDesignation)
                                        # add the data to the designation
                            if '.language' in line:
                                theDesignation.language = value
                            elif '.use' in line:
                                if 'userSelected' in line:
                                    theDesignation.use.userSelected = value
                                else:
                                    str4Coding = value
                                    if "|" in value:
                                        str4Coding = str4Coding.replace("|","^",1)
                                        str4Coding = str4Coding.replace("#","^",1)
                                    else:
                                        str4Coding = str4Coding.replace("#","^^",1)
                                    if " \"" in value:
                                        str4Coding = str4Coding.replace(" \"","^",1).replace("\"","")
                                    else:
                                        str4Coding += "^"
                                    str4Coding += "^"
                                    theDesignation.use = pcam.Coding(str4Coding)
                            elif '.value' in line:
                                theDesignation.value = value
                if '.filter' in line:
                    theFilter = None
                                # look with idInFsh for filter in the filter list of the concept
                    for oneConcept in dictConcept:
                        if oneConcept.idInFsh == line.split(']')[0]:
                            theFilter = oneConcept
                                # if none is found in the list, make one
                    if theFilter is None:
                        theFilter.idInFsh = line.split(']')[0]
                        oneConcept.filter = theFilter
                                # add the data to the designation
                    if '.property' in line:
                        theFilter.property = value
                    elif '.op' in line:
                        theFilter.op = value
                    elif '.value' in line:
                        theFilter.value = value

    def exportoConcepto(self, outfile, incHierarchyExt4VS):
        conceptList = self.get_concept()
        if len(conceptList) > 0:
            tempConcept = None
            j = -1
            k = -1
            for oneConcept in conceptList:
                #for CodeSystems
                if self.get_resource() == 'CodeSystem':
                    if hasattr(oneConcept, 'display') and oneConcept.display:
                        outfile.write('* #%s "%s"\n' % (oneConcept.code, oneConcept.display.replace('"','\\"')))
                    else:
                        outfile.write('* #%s\n' % (oneConcept.code))
                    if hasattr(oneConcept, 'definition') and oneConcept.definition:
                        outfile.write('* #%s ^definition = %s\n' % (oneConcept.code, oneConcept.definition))
                    if hasattr(oneConcept, 'designation') and oneConcept.designation:
                        i = 0
                        for oneDesignation in oneConcept.designation:
                            if hasattr(oneDesignation, 'language') and oneDesignation.language: outfile.write('* #%s ^designation[%s].language = #%s \n' % (oneConcept.code, i, oneDesignation.language))
                            if hasattr(oneDesignation, 'use') and oneDesignation.use.code:
                                tmpCoding = oneDesignation.use.codesystem
                                if tmp := oneDesignation.use.version: tmpCoding += "|"+tmp
                                tmpCoding += "#"+oneDesignation.use.code
                                if tmp := oneDesignation.use.display: tmpCoding += " \""+tmp+"\""
                                outfile.write('* #%s ^designation[%s].use = %s \n' % (oneConcept.code, i, tmpCoding))
                                if tmp := oneDesignation.use.userSelected:
                                    outfile.write('* #%s ^designation[%s].use.userSelected = %s \n' % (oneConcept.code, i, tmp))
                            if hasattr(oneDesignation, 'value') and oneDesignation.value: outfile.write('* #%s ^designation[%s].value = "%s" \n' % (oneConcept.code, i, oneDesignation.value))
                            i += 1
                    if hasattr(oneConcept, 'property') and oneConcept.property:
                        i = 0
                        for oneProperty in oneConcept.property:
                            if hasattr(oneProperty, 'code'): outfile.write('* #%s ^property[%s].code = #%s \n' % (oneConcept.code, i, oneProperty.code))
                            if hasattr(oneProperty, 'value') and oneProperty.type.value == "string": outfile.write('* #%s ^property[%s].value%s = "%s" \n' % (oneConcept.code, i, pcam.first_char_to_upper(oneProperty.type.value), oneProperty.value.replace('"','\\"')))
                            elif hasattr(oneProperty, 'value') and oneProperty.type.value == "code": outfile.write('* #%s ^property[%s].value%s = #%s \n' % (oneConcept.code, i, pcam.first_char_to_upper(oneProperty.type.value), oneProperty.value.replace('"','\\"')))
                            elif hasattr(oneProperty, 'value') and oneProperty.type.value == "boolean": outfile.write('* #%s ^property[%s].value%s = %s \n' % (oneConcept.code, i, pcam.first_char_to_upper(oneProperty.type.value), str(oneProperty.value).lower().replace('"','\\"')))
                            i += 1
                elif self.get_resource() == 'ValueSet': # e.g. ->* compose.include.concept[5].designation[4].language
                    if tempConcept == None or tempConcept.system != oneConcept.system:
                        j += 1
                        k = -1
                        tempConcept = oneConcept
                        outfile.write('* compose.include[%s].system = "%s"\n' % (j, oneConcept.system))
                        # write the system version if existent
                        if hasattr(oneConcept, 'version') and oneConcept.version:
                            outfile.write('* compose.include[%s].version = "%s"\n' % (j, oneConcept.version))
                        else:
                            # retrieve version from metadata

                            # theoretically one_concept.system could be either canonical or OID in
                            # most cases, however, it would be the canonical
                            if tmp := self.gainVersionFromCanonical(oneConcept.system):
                                outfile.write('* compose.include[%s].version = "%s"\n' % (j, tmp))
                            elif tmp := self.gainVersionFromOid(oneConcept.system):
                                outfile.write('* compose.include[%s].version = "%s"\n' % (j, tmp))

                    if hasattr(oneConcept, 'code'):
                        k += 1
                        outfile.write('* compose.include[%s].concept[%s].code = "%s"\n' % (j, k, oneConcept.code))
                    if hasattr(oneConcept, 'display') and oneConcept.display:
                        outfile.write('* compose.include[%s].concept[%s].display = "%s"\n' % (j, k, oneConcept.display))
                    if hasattr(oneConcept, 'designation') and oneConcept.designation:
                        i = -1
                        for oneDesignation in oneConcept.designation:
                            i += 1
                            if hasattr(oneDesignation, 'language') and oneDesignation.language: outfile.write('* compose.include[%s].concept[%s].designation[%s].language = #%s \n' % (j, k, i, oneDesignation.language))
                            if hasattr(oneDesignation, 'use') and oneDesignation.use.code:
                                tmpCoding = oneDesignation.use.codesystem
                                if tmp := oneDesignation.use.version: tmpCoding += "|"+tmp
                                tmpCoding += "#"+oneDesignation.use.code
                                if tmp := oneDesignation.use.display: tmpCoding += " \""+tmp+"\""
                                outfile.write('* compose.include[%s].concept[%s].designation[%s].use = %s \n' % (j, k, i, tmpCoding))
                                if tmp := oneDesignation.use.userSelected:
                                    outfile.write('* compose.include[%s].concept[%s].designation[%s].use.userSelected = %s \n' % (j, k, i, tmp))
                            if hasattr(oneDesignation, 'value') and oneDesignation.value: outfile.write('* compose.include[%s].concept[%s].designation[%s].value = "%s" \n' % (j, k, i, oneDesignation.value))
                    if hasattr(oneConcept, 'filter') and oneConcept.filter:
                        k += 1
                        for oneFilter in oneConcept.filter:
                            if hasattr(oneFilter, 'property') and oneFilter.property: outfile.write('* compose.include[%s].filter[%s].property = #%s \n' % (j, k, oneConcept.filter.property))
                            if hasattr(oneFilter, 'op') and oneFilter.op: outfile.write('* compose.include[%s].filter[%s].op = #%s \n' % (j, k, oneConcept.filter.op))
                            if hasattr(oneFilter, 'value') and oneFilter.value: outfile.write('* compose.include[%s].filter[%s].value = "%s" \n' % (j, k, oneConcept.filter.value))

                    # ToDo: implement exclude similar to include
                    # ToDo: implement simple Hierarchie for Children with the member extension